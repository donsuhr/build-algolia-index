'use strict';

const path = require('path');
const fs = require('fs').promises;
const crypto = require('crypto');
// eslint-disable-next-line import/no-extraneous-dependencies
const pdfExtract = require('pdfjs-dist');
// eslint-disable-next-line import/no-extraneous-dependencies
const FileType = require('file-type');

function gePdfPageText(pageNum, pdfDocument) {
    return pdfDocument
        .getPage(pageNum)
        .then((pageInfo) => pageInfo.getTextContent())
        .then((textBits) => textBits.items.reduce((acc, x) => `${acc} ${x.str} `, ''))
        .then((combined) => combined.replace(/\s\s+/g, ' '));
}

/**
 * Fetch metadata from file
 * @param {Object} s3itemMeta - metadata returned from api
 * @param {string} s3itemMeta.file - local file path
 * @param {string} s3itemMeta.title - title of document
 * @param {string} s3itemMeta.description - description of document
 * @return {Object}
 */
module.exports = async function getFileContentsInfo(s3itemMeta) {
    const filePath = path.resolve(
        process.cwd(),
        `./search-index/${s3itemMeta.file}`,
    );

    const lstat = await fs.stat(filePath);
    const fileData = !lstat.isDirectory() ? await fs.readFile(filePath) : '';

    const mime = fileData.length > 0
        ? (await FileType.fromBuffer(fileData)).mime
        : 'placeholder';

    const objectID = crypto
        .createHash('md5')
        .update(s3itemMeta.file)
        .digest('hex');

    let body = s3itemMeta.description;
    const fileHash = crypto
        .createHash('md5')
        .update(`${s3itemMeta.title}-${s3itemMeta.description}`);

    if (mime === 'application/pdf') {
        try {
            const pdfDocument = await pdfExtract.getDocument(fileData).promise;
            fileHash.update(fileData);
            const pagesTexts = [];
            for (let i = 1; i <= pdfDocument.numPages; i += 1) {
                pagesTexts.push(gePdfPageText(i, pdfDocument));
            }
            const pageContentArray = await Promise.all(pagesTexts);
            body = `${s3itemMeta.description} ${pageContentArray.join(' ')}`;
        } catch (e) {
            // eslint-disable-next-line no-console
            console.log('error reading', s3itemMeta.file);
            // eslint-disable-next-line no-console
            console.log(e);
        }
    }
    return {
        fileHash: fileHash.digest('hex'),
        objectID,
        body,
        type: mime,
    };
};
