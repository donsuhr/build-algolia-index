'use strict';

const fs = require('fs');
const crypto = require('crypto');
const yaml = require('js-yaml');
// eslint-disable-next-line import/no-extraneous-dependencies
const globby = require('globby');

function getFileData(file, urlRoot) {
    return new Promise((resolve, reject) => {
        fs.readFile(file, 'utf8', (rfErr, data) => {
            if (rfErr) {
                reject(rfErr);
            }
            const objectID = crypto
                .createHash('md5')
                .update(file)
                .digest('hex');
            const metaDataStart = data.indexOf('---') + 3;
            const metaDataString = data.substring(
                metaDataStart,
                data.indexOf('---', metaDataStart),
            );
            const metaData = yaml.load(metaDataString);
            const body = /{{\s*#\s*content\s*['"]body['"]\s*}}((.|\n)*?){{\s*\/\s*content\s*}}/gi.exec(
                data,
            );
            if (body && body.length > 1) {
                metaData.body = body[1].replace(/{{\s*snippet\s*}}/gi, '');
            }
            const afterBody = /{{\s*#\s*content\s*['"]after-body['"]\s*}}((.|\n)*?){{\s*\/\s*content\s*}}/gi.exec(
                data,
            );
            if (afterBody && afterBody.length > 1) {
                [, metaData.afterBody] = afterBody;
            }
            const fileHash = crypto
                .createHash('md5')
                .update(data)
                .digest('hex');

            let url = `${urlRoot}/${file.replace(
                /app\/|pages\/|\.html|\/index/gi,
                '',
            )}/`;
            url = url.replace(/index\//gi, ''); // fix home page
            resolve({
                ...metaData,
                objectID,
                file,
                fileHash,
                url,
            });
        });
    });
}

module.exports = function parseFileData(src, urlRoot) {
    return new Promise((resolve, reject) => {
        globby(src)
            .then((files) => {
                resolve(Promise.all(files.map((x) => getFileData(x, urlRoot))));
            })
            .catch((e) => {
                reject(e);
            });
    });
};
