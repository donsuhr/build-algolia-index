'use strict';

const { argv } = require('yargs'); // eslint-disable-line import/no-extraneous-dependencies

const { buildSearchIndex, buildSearchIndexDocs } = require('./index');

const searchClient = {
    initIndex() {
        return {
            deleteObjects(items) {
                // eslint-disable-next-line no-console
                console.log('delete objects');
                // eslint-disable-next-line no-console
                console.dir(items.map((x) => x.title));
            },
            saveObjects(items, opts) {
                // eslint-disable-next-line no-console
                console.log('add/save objects', opts);
                // eslint-disable-next-line no-console
                console.dir(items.map((x) => x.title));
            },
        };
    },
};

const dryRun = !!argv['dry-run'];

const groups = [
    {
        json: 'search-index--cmc--en-us.json',
        algoliaIndex: 'cmc--en-us',
        src: [
            '../cmc-site/app/pages/services/**/*.html',
            '!../cmc-site/app/pages/dev/**/*.html',
            '!../cmc-site/app/pages/demos/**/*.html',
            '!../cmc-site/app/pages/blog/**/*.html',
            '!../cmc-site/app/pages/snippets-and-examples/**/*.html',
            '!../cmc-site/app/pages/en-in/**/*.html',
        ],
    },
];

buildSearchIndex(
    groups,
    searchClient,
    dryRun,
    'https://cmc.suhrthing.com:9000',
    'https://api.suhrthing.com:9000',
);

const groupsDocs = [
    {
        json: 'search-index-docs--cmc--en-us.json',
        algoliaIndex: 'cmc--en-us',
        src: [
            {
                docType: 'general',
                site: 'cmc',
                group: '',
            },
            {
                docType: 'product-sheets',
                site: 'cmc',
                group: 'en-us',
            },
            {
                docType: 'case-studies',
                site: 'cmc',
                group: 'en-us',
            },
        ],
    },
];

buildSearchIndexDocs(
    groupsDocs,
    searchClient,
    dryRun,
    'https://cmc.suhrthing.com:9000',
    'https://api.suhrthing.com:9000',
    'cmcv3-',
);
