'use strict';

module.exports = async function updateAlgolia({
    searchClient,
    dryRun,
    searchIndex,
    adds,
    deletes,
    updates,
    indexName,
}) {
    const index = searchClient.initIndex(indexName);
    try {
        if (!dryRun) {
            await index.deleteObjects(deletes);
        }
        deletes.forEach((id) => {
            // eslint-disable-next-line no-console
            console.log(`${dryRun ? 'would have deleted' : 'deleted'} ${id}`);
            delete searchIndex[id];
        });
    } catch (e) {
        // eslint-disable-next-line no-console
        console.log('error deleting objects', e);
        throw e;
    }

    try {
        if (!dryRun) {
            await index.saveObjects(adds, {
                autoGenerateObjectIDIfNotExist: true,
            });
        }
        adds.forEach((x) => {
            // eslint-disable-next-line no-console
            console.log(`${dryRun ? 'would have added' : 'added'} ${x.url}`);
            searchIndex[x.objectID] = {
                file: x.file,
                fileHash: x.fileHash,
            };
        });
    } catch (e) {
        // eslint-disable-next-line no-console
        console.log('error adding objects', e);
        throw e;
    }

    try {
        if (!dryRun) {
            await index.saveObjects(updates);
        }
        updates.forEach((x) => {
            // eslint-disable-next-line no-console
            console.log(
                `${dryRun ? 'would have updated' : 'updated'} ${x.url}`,
            );
            searchIndex[x.objectID] = {
                file: x.file,
                fileHash: x.fileHash,
            };
        });
    } catch (e) {
        // eslint-disable-next-line no-console
        console.log('error saving objects', e);
        throw e;
    }

    // eslint-disable-next-line no-console
    console.log(
        `${dryRun ? 'dry run, would have updated:' : 'counts:'} \t ${
            adds.length
        } adds \t ${updates.length} updates \t ${deletes.length} deletes`,
    );
    return searchIndex;
};
