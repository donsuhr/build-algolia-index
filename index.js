'use strict';

const path = require('path');
const fs = require('fs').promises;
const isDate = require('lodash/isDate');
const fetch = require('node-fetch');
const { checkStatus, parseJSON } = require('fetch-json-helpers');
const getFileContentsInfo = require('./getFileContentsInfo');
const parseFileData = require('./getFileContentsInfo--html');
const updateAlgolia = require('./update-algolia');

async function loadCurrentIndexFile(filePath) {
    const indexJsonFile = path.resolve(process.cwd(), filePath);
    let indexJson;
    try {
        indexJson = JSON.parse(await fs.readFile(indexJsonFile));
    } catch (e) {
        // eslint-disable-next-line no-console
        console.error('error reading current index');
        // eslint-disable-next-line no-console
        console.error(e);
        indexJson = {};
    }
    return indexJson;
}

function getActions(items, searchIndex) {
    return items.reduce(
        (acc, item) => {
            const hidden = !!item.private
                || (Object.hasOwnProperty.call(item, 'display')
                    && item.display === false);
            const hasPublish = Object.hasOwnProperty.call(item, 'publish');
            const publish = !hasPublish
                || (hasPublish
                    && item.publish !== 'draft'
                    && item.publish !== 'private');
            const future = hasPublish
                && isDate(item.publish)
                && new Date(item.publish).getTime() > Date.now();
            const showItem = !hidden && publish && !future;
            if (acc.deletes.includes(item.objectID) && showItem) {
                if (searchIndex[item.objectID].fileHash !== item.fileHash) {
                    acc.updates.push(item);
                }
                acc.deletes.splice(acc.deletes.indexOf(item.objectID), 1);
            } else if (showItem) {
                acc.adds.push(item);
            }
            return acc;
        },
        {
            deletes: Object.keys(searchIndex),
            adds: [],
            updates: [],
            searchIndex,
        },
    );
}

/**
 * Fetch metadata for files from the cmc api
 * @param {Array.<{docType:string, site:string, group: 'en-us'}>} srcs - array of sources
 * @param {string} apiDomain - the domain of the api server
 * @param {string} bucketPrefix - optional prefix for s3 bucket
 */
function fetchDocumentMetadata(srcs, apiDomain, bucketPrefix = '') {
    return Promise.all(
        srcs.map((group) => {
            const site = group.site ? `${group.site}/` : '';
            const gr = group.group ? `${group.group}/` : '';
            return fetch(`${apiDomain}/${group.docType}/${site}${gr}`)
                .then(checkStatus)
                .then(parseJSON)
                .then((response) => response.data.map((item) => {
                    const file = `docs/${group.docType}/${site}${gr}${item.s3url}`;
                    const generalFile = `documents/${item.s3url}`;
                    const url = group.docType === 'general' ? generalFile : file;
                    let lsite = group.site ? `--${group.site}` : '';
                    const lgroup = group.group ? `--${group.group}` : '';
                    if (
                        group.docType === 'general'
                            && group.site === 'cmc'
                    ) {
                        lsite = '';
                    }
                    const prefix = bucketPrefix.trim() !== '' ? bucketPrefix : '';
                    return {
                        ...item,
                        url,
                        file: `${prefix}${group.docType}${lsite}${lgroup}/${item.s3url}`,
                    };
                }));
        }),
    );
}

function writeIndex(updatedSearchIndex, dryRun, jsonFileName) {
    const content = JSON.stringify(updatedSearchIndex, null, 4);
    if (!dryRun) {
        const indexJsonFile = path.resolve(
            process.cwd(),
            `./search-index/${jsonFileName}`,
        );
        fs.writeFile(indexJsonFile, content);
    } else {
        // eslint-disable-next-line no-console
        console.log(content);
    }
}

exports.buildSearchIndex = function buildSearchIndex(
    groups,
    searchClient,
    dryRun,
    urlRoot,
    apiURL,
) {
    groups.forEach((group) => {
        parseFileData(group.src, urlRoot)
            .then(async (result) => {
                const searchIndex = await loadCurrentIndexFile(
                    `./search-index/${group.json}`,
                );
                return getActions(result, searchIndex);
            })
            .then(({
                adds, deletes, updates, searchIndex,
            }) => updateAlgolia({
                searchClient,
                adds,
                deletes,
                updates,
                searchIndex,
                dryRun,
                indexName: group.algoliaIndex,
            }))
            .then((updatedSearchIndex) => {
                writeIndex(updatedSearchIndex, dryRun, group.json);
            });
    });
};

exports.buildSearchIndexDocs = function buildSearchIndexDocs(
    groups,
    searchClient,
    dryRun,
    urlRoot,
    apiURL,
    bucketPrefix,
) {
    groups.forEach((group) => {
        fetchDocumentMetadata(group.src, apiURL, bucketPrefix)
            .then((metadataArray) => {
                const flattened = metadataArray.reduce(
                    (prev, cur) => prev.concat(cur),
                    [],
                );
                return Promise.all(
                    flattened.map(async (item) => {
                        const contentsInfo = await getFileContentsInfo(item);
                        return {
                            ...item,
                            ...contentsInfo,
                            url: `${urlRoot}/${item.url}`,
                        };
                    }),
                );
            })
            .then(async (result) => {
                const indexJson = await loadCurrentIndexFile(
                    `./search-index/${group.json}`,
                );
                return getActions(result, indexJson);
            })
            .then(({
                adds, deletes, updates, searchIndex,
            }) => updateAlgolia({
                searchClient,
                adds,
                deletes,
                updates,
                searchIndex,
                dryRun,
                indexName: group.algoliaIndex,
            }))
            .then((updatedSearchIndex) => {
                writeIndex(updatedSearchIndex, dryRun, group.json);
            });
    });
};
